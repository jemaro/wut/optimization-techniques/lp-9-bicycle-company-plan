\section{Mathematical Model}
\label{sec:mathematical-model}

The different parameters and sets that are defined by the problem statement can
be summarized in \autoref{table:parameters}. It is also clear from this same
statement that the objective of the optimization is to minimize the total cost.
Which can be decomposed in production and storage cost. Represented in
\autoref{eqn:total-cost}.

\begin{table}[h]
    \centering
    \begin{tabular}{ l l c}
        symbol & definition & value \\ \hline
        $M$ & Number of months in the period taken into account & $12$\\
        $S$ & Set of all states taken into account & \{$normal$, $overtime$\} \\
        $xmax_s$ & Bicycle production capacity of the $s$-th state, in
        thousands & \{$30$, $15$\} \\
        $Cx_s$ & Production cost of a single bicycle of the $s$-th state in
        \texteuro & \{$32$, $40$\}\\
        $sales_m$ & Sales forecast of the $m$-th month, in thousands & \autoref{table:sales-forecast} \\
        $z_{ini}$ & Bicycles in stock at the beginning of the year, in
        thousands & $2$\\
        $Cz$ & Storage cost of a single bicycle in \texteuro & $5$\\
    \end{tabular}
    \caption{Mathematical model parameters and sets. Defined in the data file}
    \label{table:parameters}
\end{table}

\begin{equation}
    \min_x \sum_{m=1}^{M} (Costx_{m} + Costz_{m})
    \label{eqn:total-cost}
\end{equation}


The unitary price of production depends on this state so we will distinguish
the number of produced units in the different states for each month. Which will
be our \textbf{decision variables} $x_{m,s}$, being $m$ the month number and
$s$ the production state. The production cost for the $m$-th month is
represented in \autoref{eqn:production-cost} as the sum along states of the
produced $x_{m,s}$ units multiplied by its respective cost $Cx_s$.

\begin{equation}
    Costx_m = \sum_{s\in S} (Cx_s \cdot x_{m,s})\quad\text{for $m = 1, 2, ..., M$}
    \label{eqn:production-cost}
\end{equation}

The storage cost is simpler, represented in \autoref{eqn:storage-cost} is
modelled as the product of the stored units on the $m$-th month ($z_m$) and the
unitary storage cost $Cz$.

\begin{equation}
    Costz_m = Cz \cdot z_m\quad\text{for $m = 1, 2, ..., M$}
    \label{eqn:storage-cost}
\end{equation}

The number of stored units $z_m$ in a given month $m$ can be computed as the
stored units in the previous month $z_{m-1}$ plus the production-sales balance
$b_m$ of the $m$-th month, as it is represented in \autoref{eqn:storage}. A
special case is considered for the first month where the stored units in the
previous month $z_0$ is defined by the parameter $z_{ini}$ provided in the
problem statement, which corresponds to the stock at the beginning of the
period taken into account.

\begin{align}
z_0 &=  z_{ini} \\
z_m &=  z_{m-1} + b_m\quad\text{for $m = 1, 2, ..., M$}
\label{eqn:storage}
\end{align}

The production-sales balance $b_m$ of the $m$-th month is defined in
\autoref{eqn:balance} as the difference between the production $production_m$
and the sales forecast $sales_m$ of the given month $m$. Being the sales
forecast $sales_m$ a parameter provided in the problem statement whose values
can be found in \autoref{table:sales-forecast}. The $m$-th month production
$production_m$ is in turn defined in \autoref{eqn:production} as the sum along
states $s$ of the produced units $x_{m,s}$ in the given month $m$.

\begin{equation}
    b_m = production_m - sales_m\quad\text{for $m = 1, 2, ..., M$}
    \label{eqn:balance}
\end{equation}

\begin{equation}
    production_m = \sum_{s\in S} x_{m, s}\quad\text{for $m = 1, 2, ..., M$}
    \label{eqn:production}
\end{equation}

Our objective function is fully defined by now, but there are two additional
constrains to add. The production per stage is limited. There are a
maximum number of units that a state can produce in a given month. And this
number of units produced must be positive, obviously. Represented by \autoref{eqn:constrain-limited-production}.

\begin{equation}
    0 \leq x_{m,s} \leq xmax_s\quad\text{for $m = 1, 2, ..., M$ and $s$ in $S$}
    \label{eqn:constrain-limited-production}
\end{equation}

Additionally, the sales forecast must always be satisfied at each month. Which
means that the production-sales balance can only be negative if there are
stored units from previous months. It is reflected in
\autoref{eqn:constrain-satisfy-demand} with our current mathematical model.

\begin{equation}
    z_m \geq 0\quad\text{for $m = 1, 2, ..., M$}
    \label{eqn:constrain-satisfy-demand}
\end{equation}

\autoref{table:variables} summarizes the definitions of all the different
variables defined in our model.

\begin{table}[h]
    \centering
    \begin{tabular}{ l l }
        variable & definition \\ \hline
        $Costx_m$ & Cost associated with production in the $m$-th month, in
        thousands of \texteuro \\
        $x_{m,s}$ & Decision variables. Production of bicycles of the $s$-th
        state in the $m$-th month, in thousands \\
        $x$ & Array of bicycles production for the different states and months
        \\
        $Costz_m$ & Cost associated with storage in the $m$-th month, in
        thousands of \texteuro \\
        $z_m$ & Stored bicycles in the $m$-th month, in thousands \\
        $b_m$ & Balance of production versus sales of the $m$-th month, in
        thousands \\
        $production_m$ & Bicycles production of the $m$-th month, in thousands
        \\
    \end{tabular}
    \caption{Mathematical model variables.}
    \label{table:variables}
\end{table}


\paragraph{Assumptions}
\label{par:mathematical-model}
The presented mathematical model has two main assumptions.

\begin{itemize}
    \item The storage capacity is unlimited
    \item The period taken into account starts with the month $1$ and extends
    to the last month $M$. The sales forecast $sales_m$ is indexed accordingly.
\end{itemize}