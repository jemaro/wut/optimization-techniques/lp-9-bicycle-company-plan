# AMPL MODEL file for Bicycle company plan

option solver cplex;

# Declarations of sets and parameters
set MONTH;
set STATE;

param x_max {STATE};
param z_ini;
param Cx {STATE};
param Cz;
param sales {MONTH};


# Decision variable array
var x {m in MONTH, s in STATE} >= 0, <= x_max[s];

# Problem variables
var Cost_x {m in MONTH} = sum {s in STATE} Cx[s]*x[m, s];
var p {m in MONTH} = sum {s in STATE} x[m, s];
var b {m in MONTH} = p[m] - sales[m];
var z {m in MONTH} = (if m = 1 then z_ini else z[m-1]) + b[m];
var Cost_z {m in MONTH} = Cz*z[m];

# Objective
minimize Cost: sum {m in MONTH} (Cost_x[m] + Cost_z[m]);
# Constraints on allways satisfying the monthly demand
subject to SatisfyDemand {m in MONTH}: z[m] >= 0